export default (number: string): boolean => !!/^[0-9]+$/.exec(number)
