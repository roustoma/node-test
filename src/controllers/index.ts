import Router from 'koa-router'

import { AppContext, AppState } from '../app'
import statusRouter from './status'
import customerRouter from './admin/customer'
import userRouter from './admin/user'


const apiRouter = new Router<AppState, AppContext>({
  prefix: '/api/v1',
})

apiRouter
  .use('/status', statusRouter)
  .use('/admin/product', customerRouter)
  .use('/admin/user', userRouter)

export default apiRouter.routes()