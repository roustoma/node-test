import Router from 'koa-router'

import { AppContext, AppState } from '../app'

const router = new Router<AppState, AppContext>()

router.get('/', (ctx) => {
  ctx.body = 'Server is running'
})

export default router.routes()