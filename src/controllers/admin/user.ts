import createHttpError from 'http-errors'
import Router from 'koa-router'


import { AppContext, AppState } from '../../app'
import isPositiveInteger from '../../utils/isPositiveInteger'
import * as userService from '../../services/user'

const router = new Router<AppState, AppContext>()

router.get('/', async (ctx) => {
  ctx.body = await userService.listAll(ctx.db)
})

router.post('/', (ctx) => {
  ctx.body = 'customer add'
})

router
  .param('id', async (id, ctx, next) => {
    if (!isPositiveInteger(id)) {
      throw createHttpError(404, 'Not a valid ID in URL parameter')
    }
    await next()
  })

  .get('/:id', (ctx) => {
    ctx.body = 'customer detail'
  })

  .patch('/:id', (ctx) => {
    ctx.body = 'customer update'
  })

export default router.routes()