import Router from 'koa-router'

import { AppContext, AppState } from '../../app'

const router = new Router<AppState, AppContext>()

router.get('/', (ctx) => {
  ctx.body = 'Customer Admin Server is running'
})


// * * * CATEGORIES
// * * * CATEGORIES
// * * * CATEGORIES


router.get('/categories', (ctx) => {
  ctx.body = 'all categories'
})


router.get('/categories/:id', (ctx) => {
  ctx.body = 'category detail'
})

router.patch('/categories/:id', (ctx) => {
  ctx.body = 'category update'
})

router.post('/categories/:id', (ctx) => {
  ctx.body = 'category add'
})

router.delete('/categories/:id', (ctx) => {
  ctx.body = 'category delete' 
  // set deleted = True
})

// * * * PRODUCTS
// * * * PRODUCTS
// * * * PRODUCTS


router.get('/products', (ctx) => {
  ctx.body = 'All products'
})


router.get('/products/:id', (ctx) => {
  ctx.body = 'Product detail'
})

router.patch('/products/:id', (ctx) => {
  ctx.body = 'Product update'
})

router.post('/products/:id', (ctx) => {
  ctx.body = 'Product add'
})






export default router.routes()