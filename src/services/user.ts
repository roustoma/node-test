import { Knex } from 'knex'

import userRepository from '../repository/user'
import { User } from '../types/user'


export const listAll = async (db: Knex): Promise<User[]>=> {
  return await userRepository.listAll(db)
}