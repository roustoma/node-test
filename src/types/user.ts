export interface User {
  id: number
  name: string
  last_name: string
  active: boolean
}