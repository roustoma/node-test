import createHttpError from "http-errors"
import { Next } from "koa"
import { AppContext } from "../app"

export default async function (ctx: AppContext, next: Next): Promise<void> {
  try {
    await next()
  } catch (err) {
    if (err instanceof createHttpError.HttpError) {
      ctx.status = err.status,
      ctx.body = {
        title: err.message,
        status: err.status
      }
    } else {
      ctx.status = 500
      ctx.body = {
        title: 'Internal server error',
        status: 500
      }
    }
    ctx.app.emit('error', err, ctx)
  }
}