import { Knex } from 'knex'

import { User } from '../types/user'

export default {
  listAll: async (db: Knex): Promise<User[]> => {
    return await db<User>('customer').select('*')
  },
}